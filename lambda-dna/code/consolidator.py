import numpy as np
from glob import glob
import os
import itertools

### Assumes that Espresso only outputs at the end of its run!

### Check data folder for output files
### For each one found, read the contents into the master data array,
### Then delete the file

## File names
fnames = glob('data/*_chops.dat')

## Summary array. Fields for each file are
##........Seed           int
##........Case Number    int
##........box_l          int
##........N              int
##........Rmax           float
##........Chops          list of 2-tuples
try:
    summary = np.load('summary.npy')
except IOError:
    summary = np.ndarray((0,1),
                         dtype=[('rseed',  'uint64'),
                                ('casenum','uint64'),
                                ('box_l',  'uint64'),
                                ('N',      'uint64'),
                                ('Lambda', 'float64'),
                                ('Rmax',   'float64'),
                                ('chops','uint64', (400,2))])


## Process, package, then delete files
# Object for each case
thiscase = np.zeros((1,1),
                    dtype=[('rseed',  'uint64'),
                           ('casenum','uint64'),
                           ('box_l',  'uint64'),
                           ('N',      'uint64'),
                           ('Lambda',   'float64'),
                           ('Rmax',   'float64'),
                           ('chops','uint64', (400,2))])

# Iterate through files
for fname in fnames:

    # Parse name
    basename       = os.path.basename(fname)
    basename_array = basename.split('_')
    rseed =   int(basename_array[0])
    casenum = int(basename_array[1])
    box_l =   int(basename_array[2])
    N =       int(basename_array[3])
    Lambda =  float(basename_array[4])
    Rmax =    float(basename_array[5])

    # Log parameters
    thiscase['rseed'] =   rseed
    thiscase['casenum'] = casenum
    thiscase['box_l'] =   box_l
    thiscase['N'] =       N
    thiscase['Lambda'] =  Lambda
    thiscase['Rmax'] =    Rmax

    # Read file, log chops
    with open(fname,'r') as fin:

        chops = np.zeros((400,2),dtype=np.uint64)

        for i, line in itertools.izip(itertools.count(),fin):

            line = line.split()

            if (len(line) > 0):
                chops[i,0] = int(line[0])
                chops[i,1] = int(line[1])

    thiscase['chops'] = np.reshape(chops,(1,1,400,2))

    # Add to summary
    summary = np.vstack([summary,thiscase])

    # Move file to purgatory
    os.rename(fname,fname.replace('data/','purgatory/'))


## Save summary
summary.dump('summary.npy')





























