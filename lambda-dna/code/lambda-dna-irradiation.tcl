# Example input:
# ~/espresso-2.1.2j_modfene/Espresso lambda-dna-irradiation.tcl 1234 1 30 400 0.5 0.5

# Parse input
set rseed        [lindex $argv 0]
set nevents      [lindex $argv 1]
set box_l        [lindex $argv 2]
set N            [lindex $argv 3]
set lambda       [lindex $argv 4]
set Rmax         [lindex $argv 5]
t_random seed    $rseed
set first [expr srand($rseed)]

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { rseed casenum box_l N lambda Rmax } {
    global VMD_IS_ON 
    # Manually control visualization
    set vis_flag 1

    # Scales
    set PI 3.14159
    set sig 1.0
    set eps 1.0

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # LJ Potential Parameters (between repair enzymed ends)
    set eps_repair   10.
    set sig_repair   2.
    set cut_repair   10.
    set shift_repair 0.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # Angular Potential Parameters
    set K 5.
    set phi0 3.14159

    # Probability parameters
    set p_enzymed 1.0
    set t_enzymed 0

    # Irradiate only near the center
    

    ##########################################################################


    # Spatial domain creation
    setmd box_l $box_l $box_l $box_l
    setmd period 0 0 0

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    # Currently shifted FENE - eventually modified FENE-WCA
    inter 0 fene $kap $lam
    inter 7 angle $K $phi0
    inter 2 2 lennard-jones $eps_repair $sig_repair $cut_repair $shift_repair 0.

    # Fix center of mass
    inter 0 0 comfixed 1    

    # Polymer creation
    set x [expr {$box_l/2.}]
    set y [expr {$box_l/2.}]
    set z [expr {$box_l/2.}]
    for { set i 0 } { $i < $N } { incr i } {
	# Increment position
	# Use polar coordinates, ITS
	# f(phi) = 1/2pi -> F(phi) = phi/2pi -> Rphi = 2pi*Runi
	# f(theta) = sin(theta)/2 -> F(theta) = (1-cos(theta))/2 -> Rtheta = acos(1-2*Runi)

	set r $sig
	set rnd1 [expr rand()]
	set rnd2 [expr rand()]
	set phi [expr {2.*$PI*$rnd1}]
	set theta [expr {acos(1.-2.*$rnd2)}]

	set dx [expr {$r*sin($theta)*cos($phi)}]
	set dy [expr {$r*sin($theta)*sin($phi)}]
	set dz [expr {$r*cos($theta)}]

	set x [expr {$x + $dx}]
	set y [expr {$y + $dy}]
	set z [expr {$z + $dz}]

	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.

	# FENE bond to the previous particle in the chain
	if { $i > 0 } {
	    part $i bond 0 [expr {$i - 1}] 
	}

	# Angular band with two previous particles
	if { $i > 1 } {
	    part [expr {$i-1}] bond 7 [expr {$i-2}] $i
	}

	set timer($i) 0
    }

    # Shift center of mass to center of domain
    set COM [analyze centermass 0]
    set cx [lindex $COM 0]
    set cy [lindex $COM 1]
    set cz [lindex $COM 2]
    for { set i 0 } { $i < $N } { incr i } {
	set curpos [part $i print pos]
	set x [lindex $curpos 0]
	set y [lindex $curpos 1]
	set z [lindex $curpos 2]
	part $i pos [expr {$x - $cx + ($box_l/2.)}] [expr {$y - $cy + ($box_l/2.)}] [expr {$z - $cz + ($box_l/2.)}]
    }

    # Marker polymer
    part $N pos 0 0 0 type 1 fix 1 1 1

    ##########################################################################


    # Initialize Visualization
    # Currently cannot visualize types properly in VMD
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate 3e5 steps
    puts "Equilibrating..."
    thermostat langevin $temp 0.1
    set eqlstepsize [expr {(1./36.)*(0.1)*pow($N,2.2)/100.}]
    set eqlstepsize [expr {int(round($eqlstepsize))}]
    puts $eqlstepsize
    for {set i 1} {$i < 1e2} {incr i 1} {
	integrate [expr {2*$eqlstepsize}]
	if { $vis_flag == 1 } {
	    imd positions
	}
	puts "Equilibration $i / 100"
    }
    puts "Equilibrated"

    # Simulate
    thermostat langevin $temp $gamma
    set timestep 0
    set num_chains 1
    while {$num_chains < $N} {

	# Repair enzymes finding free ends
	for { set i 0 } { $i < $N } { incr i } {
	    set ptype [part $i print type]
	    if { $ptype == 1 } {
		set rnd [expr rand()]
		if { $rnd < $p_enzymed } {
		    incr timer($i)
		    if { $timer($i) > $t_enzymed } {
			part $i type 2
			puts "Fixing Particle $i"
		    }
		}
	    }
	}

	# Bubble
	set bubble_roll [expr {rand()}]
	if { $bubble_roll < $lambda } {
	    set bubble_center_x [expr {$box_l*rand()}]
	    set bubble_center_y [expr {$box_l*rand()}]
	    set bubble_center_z [expr {$box_l*rand()}]
	    part $N pos $bubble_center_x $bubble_center_y $bubble_center_z 
	    set bubble_center [list $bubble_center_x $bubble_center_y $bubble_center_z]
	    set bubble_radius   [expr $Rmax]
	    for { set i 0 } { $i < $N } { incr i } {
		# Get the position of the ith particle
		# Folded position?
		set r_0 [part $i print folded_position]

		# If it is inside the bubble,
		# delete bonds, which leaves it bonded to the next ball
		# Cannot activate the original ends of the strain this way
		set dist_to_center [veclen [vecsub $bubble_center $r_0]]
		if { $dist_to_center < $bubble_radius } {
		    if { [llength [lindex [part $i print bonds] 0]] > 0 } {
			puts "Broke Particle $i"
			# Record the break
			lappend choplist "$timestep $i"

			# Delete bond to previous particle
			part $i bond delete

			# Change types to broken end, without enzymes
			part $i type 1
			part [expr {$i-1}] type 1

			# Count chains
			incr num_chains
		    }
		}
	    }
	}

	# Integrate 
	integrate 100
	incr timestep 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    # Output files
    set CaseName "${rseed}_${casenum}_${box_l}_${N}_${lambda}_${Rmax}"
    set Fchops   [open "data/${CaseName}_chops.dat"   "w"]
    for { set i 0 } { $i < $N } { incr i } {
	puts $Fchops [lindex $choplist $i]
    }

    close $Fchops
    return 0
}


##########################################################################
##########################################################################

# Actually run the program
# Run nevents translocations
for { set casenum 1 } { $casenum <= $nevents } { incr casenum } {

    RunMain $rseed $casenum $box_l $N $lambda $Rmax

    # Unbond the polymer (so the function can be called again)
    for { set i 1 } { $i < $N } { incr i } {
	part $i bond delete
    }   

}


