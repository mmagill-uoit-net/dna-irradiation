# Example input:
# ~/espresso-2.1.2j/Espresso dna-irradiation.tcl 1234 1 20 400 0.5 0.5

# Parse input
set rseed        [lindex $argv 0]
set nevents      [lindex $argv 1]
set box_l        [lindex $argv 2]
set N            [lindex $argv 3]
set lambda       [lindex $argv 4]
set Rmax         [lindex $argv 5]
t_random seed    $rseed

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { rseed casenum box_l N lambda Rmax } {
    global VMD_IS_ON 
    # Manually control visualization
    set vis_flag 1

    # Scales
    set sig 1.0
    set eps 1.0

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]

    ##########################################################################


    # Spatial domain creation
    # Compensate for 1u wall at each wall
    set box_l [expr {$box_l+2}]
    setmd box_l $box_l $box_l $box_l

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.
    inter 0 76 lennard-jones $eps $sig $cut $shift 0.

    # Put walls on the walls
    constraint plane cell  1 -1 -1 type 76
    constraint plane cell -1  1 -1 type 76
    constraint plane cell -1 -1  1 type 76
    constraint plane cell [expr {$box_l-1}] -1 -1 type 76
    constraint plane cell -1 [expr {$box_l-1}] -1 type 76
    constraint plane cell -1 -1 [expr {$box_l-1}] type 76

    # Polymer creation (corner-to-corner across cube)
    set x 2.
    set y 2.
    set z 2.
    for { set i 0 } { $i < $N } { incr i } {
	# Increment position
	set x [expr {$x + 0.6}]
	set y [expr {$x + 0.6}]
	set z [expr {$z + 0.6}]

	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.

	# FENE bond to the previous particle in the chain
	if { $i > 0 } {
	    part $i bond 0 [expr {$i - 1}] 
	}
    }

    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate (fix all threaded monomers)
    puts "Equilibrating..."
    thermostat langevin $temp 0.1
    for {set i 1} {$i < 1e4} {incr i 1} {
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    puts "Equilibrated"

    # Simulate
    thermostat langevin $temp $gamma
    set timestep 0
    set num_chains 1
    while {$num_chains < $N} {

	# Bubble
	set bubble_roll [expr {rand()}]
	if { $bubble_roll < $lambda } {
	    set bubble_center_x [expr {$box_l*rand()}]
	    set bubble_center_y [expr {$box_l*rand()}]
	    set bubble_center_z [expr {$box_l*rand()}]
	    set bubble_center [list $bubble_center_x $bubble_center_y $bubble_center_z]
	    set bubble_radius   [expr $Rmax]
	    for { set i 0 } { $i < $N } { incr i } {
		# Get the position of the ith particle
		# Folder position?
		set r_0 [part $i print folded_position]

		# If it is inside the bubble,
		# delete bonds, which leaves it bonded to the next ball
		set dist_to_center [veclen [vecsub $bubble_center $r_0]]
		if { $dist_to_center < $bubble_radius } {
		    if { [llength [lindex [part $i print bonds] 0]] > 0 } {
			lappend choplist "$timestep $i"
			part $i bond delete
			incr num_chains
		    }
		}
	    }
	}

	# Count number of chains (not necessary)
	# set num_chains 1
	# for { set i 1 } { $i < $N } { incr i } {
	#     set bonds [part $i print bonds]
	#     if { [llength [lindex $bonds 0]] == 0 } {
	# 	incr num_chains
	#     }
	# }
	# puts "$num_chains"

	# Integrate 
	integrate 100
	incr timestep 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    # Output files
	     set CaseName "${rseed}_${casenum}_${box_l}_${N}_${lambda}_${Rmax}"
    set Fchops   [open "data/${CaseName}_chops.dat"   "w"]
    for { set i 0 } { $i < $N } { incr i } {
	puts $Fchops [lindex $choplist $i]
    }

    close $Fchops
    return 0
}


##########################################################################
##########################################################################

# Actually run the program
# Run nevents translocations
for { set casenum 1 } { $casenum <= $nevents } { incr casenum } {

    RunMain $rseed $casenum $box_l $N $lambda $Rmax

    # Unbond the polymer (so the function can be called again)
    for { set i 1 } { $i < $N } { incr i } {
	part $i bond delete
    }   

}


