import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.optimize as opt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


### Global variables

summary = np.load('summary.npy')

ListOfUnique__box_l = np.unique(summary['box_l'])
ListOfUnique__N     = np.unique(summary['N'])
ListOfUnique__Lambda  = np.unique(summary['Lambda'])
ListOfUnique__Rmax  = np.unique(summary['Rmax'])


### Diagnostics

def PrintSampleRates():
    # Summarize how many cases are in each bin
    print "Sample rate by box_l values:"
    for box_l in ListOfUnique__box_l:
        print("\tbox_l = %d : \t %d" % 
              (box_l,
               summary[np.where(summary['box_l']==box_l)].shape[0]))
    print "Sample rate by N values:"
    for N in ListOfUnique__N:
        print("\tN = %d : \t %d" % 
              (N,
               summary[np.where(summary['N']==N)].shape[0]))
    print "Sample rate by Lambda values:"
    for Lambda in ListOfUnique__Lambda:
        print("\tLambda = %.1f : \t %d" % 
              (Lambda,
               summary[np.where(summary['Lambda']==Lambda)].shape[0]))
    print "Sample rate by Rmax values:"
    for Rmax in ListOfUnique__Rmax:
        print("\tRmax = %.1f : \t %d" % 
              (Rmax,
               summary[np.where(summary['Rmax']==Rmax)].shape[0]))



### Functions for Numchains Versus Time (NVT)

## Computing NVTs

def TurnChoplistInto_NumchainsVersusTime(chops):

    # Remove dummy dimension, padded zeros
    chops = np.squeeze(chops)
    chops = chops[np.nonzero(chops[:,0])]

    # Declare time series
    NumchainsVersusTime = np.zeros(chops.shape,dtype=int)

    NumchainsVersusTime[:,0] = chops[:,0]
    NumchainsVersusTime[:,1] = np.arange(1,NumchainsVersusTime.shape[0]+1,1)
        
    return NumchainsVersusTime


def AverageNvtForManyCases(cases):
    # Shouldn't be getting choptimes twice: inefficient
    # But the alternative is to make a new struct, I think

    # Set resolution
    nrows = 1000

    # Find maximum timestep across all cases
    tmax = 1
    for case in cases:
        
        # Get and clean up chop times
        choptimes = case['chops']
        choptimes = choptimes[:,0]
        choptimes = choptimes[np.nonzero(choptimes)]
        choptimes = np.squeeze(choptimes)

        # Get max
        tmax = np.max([tmax,choptimes[-1]])

    # Declare times at which to compute averages
    nvt_avg = np.zeros((nrows,2),dtype=float)
    nvt_avg[:,0] = np.linspace(0,tmax+1,nrows)
    
    for case in cases:

        # Get and clean up chop times
        choptimes = case['chops'][:,0]
        choptimes = choptimes[np.nonzero(choptimes)]
        choptimes = np.squeeze(choptimes)

        # Manually add to the un-normalized average
        nextchop = choptimes[0]
        numchains = 1
        for i in range(nrows):
            
            if ( nvt_avg[i,0] > nextchop ):
                numchains += 1
                if ( numchains < len(choptimes) ):
                    nextchop = choptimes[numchains-1]
                else:
                    nextchop = nvt_avg[-1,0]+1

            nvt_avg[i,1] += numchains
            
    nvt_avg[:,1] = nvt_avg[:,1] / float(len(cases))

    return nvt_avg



## Plotting NVTs

def Plot_NumchainsVersusTime(nvt):

    # Plot actual data
    plot = plt.step(nvt[:,0],nvt[:,1],where='post')

    # Fit actual data, then plot fit
    # Fitting needs to be on re-scaled data or nonsense occurs
    Nlim = nvt[-1,1]
    def func(t,A,k):
        return Nlim - A*np.exp(-k*t)
    popt, pcov = opt.curve_fit(func,
                               nvt[:,0]/1e8,
                               nvt[:,1])
    fit = plt.plot(nvt[:,0],
                   func(nvt[:,0],popt[0],popt[1]/1e8),
                   '--k')

    return plot, fit


def PlotNVTs_SingleCondition(box_l,N,Lambda,Rmax):
    cases = summary[np.where(
        (summary['box_l']==box_l)&
        (summary['N']==N)&
        (summary['Lambda']==Lambda)&
        (summary['Rmax']==Rmax)
    )]
    nvt_avg = AverageNvtForManyCases(cases)
    plot, fit = Plot_NumchainsVersusTime(nvt_avg)
    return plot, fit


def PlotNVTs_VaryingLambda(box_l,N,Rmax):
    for Lambda in ListOfUnique__Lambda:
        PlotNVTs_SingleCondition(box_l,N,Lambda,Rmax)
        plot[0].set_label('Lambda = %.1f' % Lambda)
    plt.title('box_l = %d, N = %d, Rmax = %.1f' % (box_l,N,Rmax))
    plt.legend(loc='lower right') 
    plt.xlabel('Time')
    plt.ylabel('Number of Chains')
    plt.ylim([1,N])
    plt.grid()
    plt.show()



def PlotNVTs_VaryingParam(param,box_l=0,N=0,Lambda=0,Rmax=0):

    # Before varying param, make the title name
    # The varied param will have value 0
    TheTitle = ('box_l = %d, N = %d, Lambda = %.1f, Rmax = %.1f' %
                (box_l,N,Lambda,Rmax))

    # Plot_SingleCondition for each variation of the param
    TheForLine = "for %s in ListOfUnique__%s:\n\t" % (param,param)
    TheLoop = TheForLine + \
              "plot, fit = PlotNVTs_SingleCondition(box_l,N,Lambda,Rmax)"+\
              ("\n\tplot[0].set_label('%s = %%.1f' %% %s)\n" %
               (param,param))
    exec(TheLoop)

    # Finalize the plot
    plt.title(TheTitle)
    plt.legend(loc='lower right') 
    plt.xlabel('Time')
    plt.ylabel('Number of Chains')
    plt.xlim(left=1e5)
    plt.ylim([1,N])
    plt.grid()
#    plt.show()


### Functions for analysing NVTs

## Compute Half-life

def Calc_HalfLifeOfNvt(nvt):
    # Compute the half-length
    # Nlim is N-1
    Nlim = nvt[-1,1]
    Nhalf = round(float(Nlim)/2.)

    # Find all times at which N=Nhalf
    HalfTimes = nvt[np.where(nvt[:,1]>=Nhalf)][0]

    # Return first time where N=Nhalf
    return HalfTimes[0]

## Plot Half-lives one way
def PlotHalfLives_AgainstLambda_VaryingRmax(box_l,N):

    for Rmax in ListOfUnique__Rmax:
        halflives = np.ndarray([len(ListOfUnique__Lambda),])
        for i in range(len(ListOfUnique__Lambda)):
            Lambda = ListOfUnique__Lambda[i]            
            cases = summary[np.where(
                (summary['box_l']==box_l)&
                (summary['N']==N)&
                (summary['Lambda']==Lambda)&
                (summary['Rmax']==Rmax)
            )]
            nvt_avg = AverageNvtForManyCases(cases)
            halflife = Calc_HalfLifeOfNvt(nvt_avg)
            halflives[i] = halflife
        case_label = 'Rmax = %.1f' % Rmax
        plt.plot(ListOfUnique__Lambda,halflives,label=case_label)

    plt.legend()
    plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Lambda')
    plt.ylabel('Half Life')
    plt.show()
            
## Plot Half-lives the opposite way
def PlotHalfLives_AgainstRmax_VaryingLambda(box_l,N):
    fig, ax = plt.subplots()

    for Lambda in ListOfUnique__Lambda:
        halflives = np.ndarray([len(ListOfUnique__Rmax),])
        for i in range(len(ListOfUnique__Rmax)):
            Rmax = ListOfUnique__Rmax[i]
            cases = summary[np.where(
                (summary['box_l']==box_l)&
                (summary['N']==N)&
                (summary['Lambda']==Lambda)&
                (summary['Rmax']==Rmax)
            )]
            nvt_avg = AverageNvtForManyCases(cases)
            halflife = Calc_HalfLifeOfNvt(nvt_avg)
            halflives[i] = halflife
        case_label = 'Lambda = %.1f' % Lambda
        plt.plot(ListOfUnique__Rmax,halflives,label=case_label)

    plt.legend()
    plt.grid(which='both')
    plt.xlim([ListOfUnique__Rmax[0],ListOfUnique__Rmax[-1]])
    plt.xscale('log')
    ax.set_xticks(ListOfUnique__Rmax)
    ax.get_xaxis().get_major_formatter().labelOnlyBase = False
    ax.get_xaxis().set_major_formatter(FormatStrFormatter('%.1f'))
    plt.yscale('log')
    plt.xlabel('Rmax')
    plt.ylabel('Half Life')
    plt.show()
            

    









