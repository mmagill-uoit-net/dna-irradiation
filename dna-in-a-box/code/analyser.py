
import loader
reload (loader)
from loader import *

import scipy.optimize as opt

## Check sample rates
PrintSampleRates()


## Default values (based on sample rates)
box_l = ListOfUnique__box_l[0]
N = ListOfUnique__N[0]
Lambda = ListOfUnique__Lambda[0]
Rmax = ListOfUnique__Rmax[0]


## Plot something
PlotHalfLives_AgainstLambda_VaryingRmax(box_l,N)
PlotHalfLives_AgainstRmax_VaryingLambda(box_l,N)

#PlotNVTs_VaryingParam('Lambda',box_l=box_l,N=N,Rmax=Rmax)

#for Rmax in ListOfUnique__Rmax:
#    PlotNVTs_VaryingParam('Lambda',box_l=box_l,N=N,Rmax=Rmax)
#    fit_and_show()

#for Lambda in ListOfUnique__Lambda:
#    PlotNVTs_VaryingParam('Rmax',box_l=box_l,N=N,Lambda=Lambda)
#    fit_and_show()



#plt.xscale('log')
#plt.yscale('log')

