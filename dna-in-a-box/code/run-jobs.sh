#!/bin/bash


# Constant parameters
rseed_suffix=123456
nevents=5
time=3000m


#box_l=30
#N=30
#    for lambda in 0.3 0.6 0.9
#    do
#	for Rmax in 0.4 0.5 0.6
#	do

N=30
lambda=0.6
Rmax=0.5

for rseed_prefix in {1..50}
do

    for box_l in 20 25
    do
	rseed=${rseed_prefix}${rseed_suffix}
	sqsub -q serial -o log.log -r $time ~/espresso-2.1.2j/Espresso dna-irradiation.tcl $rseed $nevents $box_l $N $lambda $Rmax
    done

done









